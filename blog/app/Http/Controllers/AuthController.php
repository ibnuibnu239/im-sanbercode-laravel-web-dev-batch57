<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function ShowRegister()
    {
        return view('register');
    }

    public function welcome(Request $request)
    {
        $namaDepan = $request->input('fnama');
        $namaBelakang = $request->input('lnama');

        return view('welcome', ["namaDepan" => $namaDepan, "namaBelakang" => $namaBelakang]);
    }
}
