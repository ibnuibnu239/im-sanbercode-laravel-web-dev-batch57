// Looping Pertama
console.log("looping pertama");
var mulai = 2

while(mulai <= 20) {
    console.log(mulai + " - I Love coding ")
    mulai += 2
}

// Looping Kedua
console.log("looping kedua");
var mulai = 20

while(mulai >= 2) {
    console.log(mulai + " - I will become a mobile developer ")
    mulai -= 2
}